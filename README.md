# Test assessment for Unity candidates
**Note** : Please use *Unity 2018.4.5f1* or newer for the assignment.

- Please, concentrate on data structures and architecture of the game. UX, usability and prettiness are not important at all.
- **If you don't implement all features, it's fine. Main goal for us is to see an architecture behind.**
- **Please provide a description explaining why you did this task the way you did (why did you choose certain architecture, UI management, data structures, etc) and what would you do differently if you would have more time.**
- Also submit an APK of the project, along with your project files submission.

## Desired set of features
**Goal**: Implement a game similar to Word Boggle game in Unity

- Menu with options to select between two modes "Endless" and "Levels"


**Endless Mode**
- In Endless mode the player is shown a 4x4 grid of letters.
- Player can make words by dragging to select the letters one by one.
- The Player is scored based on the letters used for the word.
- Average score per word and total score should be shown in the UI.
- When the player makes a word, the letters used for the word are removed from the grid and the new letters enter the grid from the top.

**Levels Mode**
- Similar to the endless mode but the player is given fixed objectives and a static grid (letters don't disappear when the player makes a word).
- Some tiles have bonus rewards (bugs) which can be collected by the player by making words using the bonus letter.
- Some tiles are blocked by rocks which can be unblocked by making words adjacent to the blocked letters.
- Using levelData.json provided with the project is optional.
- Different level types:
    1.  Make x words.
    2.  Reach x score in y time.
    3.  Make x words in y time.

**Bonus**
- Add these features if you have extra time:
    1. When new letters come from the top in endless mode make sure there are possible words in the newly formed grid.
    2. Level generator: Generate random levels similar to the provided levelData.json making sure that the generated levels are solvable.

## Assets Provided
- A "letterTile" prefab with the sprites for normal, blocked and bonus states. Score indicator (dots) to depict points is also present in the tile prefab.
- wordList.txt to verify if the word made by the player is valid or not.
- levelData.json, Sample level data
`{
      "bugCount": 0,
      "wordCount": 2,
      "timeSec": 0,
      "totalScore": 0,
      "gridSize": {
        "x": 3,
        "y": 3
      },
      "gridData": [
        {
          "tileType": 0,
          "letter": "W"
        },
        {
          "tileType": 0,
          "letter": "O"
        },
        {
          "tileType": 0,
          "letter": "R"
        },
        {
          "tileType": 0,
          "letter": "A"
        },
        {
          "tileType": 0,
          "letter": "N"
        },
        {
          "tileType": 0,
          "letter": "D"
        },
        {
          "tileType": 0,
          "letter": "F"
        },
        {
          "tileType": 0,
          "letter": "U"
        },
        {
          "tileType": 0,
          "letter": "N"
        }
      ]
    }`